package com.jthink.wikitreebio;

import org.gedcom4j.model.*;
import org.gedcom4j.parser.GedcomParser;
import org.gedcom4j.parser.GedcomParserException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Paul on 12/05/2016.
 */
public class WikiTreeBio
{
    public static File outputFolder;
    public static void main(String[] args) throws IOException, GedcomParserException
    {
        if(args.length==0)
        {
            System.out.println("Usage:");
            System.out.println("   wikitreebio gedcomfile [outputfolder]");
            return;
        }
        //Gedcom is first parameter
        Path path = Paths.get(args[0]);
        GedcomParser gp = new GedcomParser();
        gp.load(path.toString());
        Gedcom g = gp.gedcom;

        //If have 2nd paramter this is folder to write biographies to
        if(args.length>=2)
        {
            outputFolder = new File(args[1]);
        }

        //Marriages
        /*for(Family f:g.families.values())
        {
            System.out.println(f);
            for (Event event : f.events)
            {
                System.out.println("FaamilyEvent:" + event);
            }
        }
        System.out.println("Multimedia:"+g.multimedia.size());
*/
        //First go through all the individual events and attributes to find all citations with or without sources and roll
        //all the details in at citation level
        int i =0;
        for (Individual next : g.individuals.values())
        {
            i++;
            try
            {
                System.err.println("--"+ i + ":" + next.formattedName());
                Biographer b = new Biographer(next);
                b.createBiography();
                System.err.println("--"+ i + ":" + next.formattedName()+":done");
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }

        }


    }




}
