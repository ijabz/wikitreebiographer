package com.jthink.wikitreebio;

import com.google.common.base.Strings;
import org.gedcom4j.model.Note;

import java.util.List;
import java.util.Objects;

/**
 * Created by Paul on 12/05/2016.
 */
public class AdvancedCitation extends Object
{
    private String xref;
    private String title;
    private String details;
    private List<Note> notes;
    private String url;

    public AdvancedCitation(String xref, String title, String details, List<Note> notes)
    {
        this.xref=xref;
        this.title=Formatting.makeReferenceName(title);
        if(!Strings.isNullOrEmpty(details))
        {
            this.details = details;
        }
        else
        {
            this.details=title;
        }
        this.notes=notes;
        if(notes!=null && notes.size()>0)
        {
            loop1:for(Note note:notes)
            {
                for(String line:note.lines)
                {
                    if(line.startsWith("http://"))
                    {
                        url=line;
                        System.out.println("   Found weburl:"+line);
                        break loop1;
                    }
                }
            }

        }

    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDetails()
    {
        return details;
    }

    public void setDetails(String details)
    {
        this.details = details;
    }

    public String getXref()
    {
        return xref;
    }

    public void setXref(String xref)
    {
        this.xref = xref;
    }

    public String getUrl()
    {
        return url;
    }

    @Override
    public boolean equals(Object obj)
    {
        return (this.getXref() == ((AdvancedCitation)obj).getXref());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(xref);
    }
}
