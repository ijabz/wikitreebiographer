package com.jthink.wikitreebio;

/**
 * Created by Paul on 12/05/2016.
 */
public class Formatting
{
    /**
     * title text
     *
     * @param string
     * @return
     */
    public static String t(String string)
    {
        return "\n\n" + "'''"+ string + ":'''";
    }


    /**
     * subtitle text
     *
     * @param string
     * @return
     */
    public static String st(String string)
    {
        return "\n''"+ string + ":''";
    }

    /**
     * Indent text
     *
     * @param string
     * @return
     */
    public static String i(String string)
    {
        return "\n:"
                + string
                .replace(", ,",",")
                .replaceAll("([A-Za-z]+),([A-Za-z]+)","$1, $2")
                .replaceAll("([A-Za-z]+),([A-Za-z]+)","$1, $2")
                + '.';
    }

    /**Subtitle and indent
     *
     * @param string
     * @return
     */
    public static String sti(String string)
    {
        return "\n:''"+ string.replaceAll(",,",",") + ":''";
    }

    /**
     * Remove spaces for ref names
     *
     * @param string
     * @return
     */
    public static String makeReferenceName(String string)
    {
        String s =  string.replaceAll(" ", "").replaceAll(",", "").replaceAll("_","");
        return s;
    }

}
