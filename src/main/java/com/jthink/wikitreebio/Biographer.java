package com.jthink.wikitreebio;

import com.google.common.base.Strings;
import org.gedcom4j.model.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import static com.jthink.wikitreebio.Formatting.*;

/**
 * Creates Biography.
 */
public class Biographer
{
    private Set<AdvancedCitation> citations = new HashSet<AdvancedCitation>();
    private Individual individual;

    public Biographer(Individual individual)
    {
        this.individual =individual;
    }

    public String createFilename()
    {
        StringBuilder sb = new StringBuilder();

        PersonalName pn = individual.names.get(0);
        System.err.println("---"+pn.givenName.value + ":" +pn.basic + ":" +pn.surname.value);
        sb.append(pn.givenName.value + " " + pn.surname.value);

        for (IndividualEvent b : individual.getEventsOfType(IndividualEventType.BIRTH)) {
            sb.append(", b.");
            sb.append(b.date);
            break;
        }
        for (IndividualEvent d : individual.getEventsOfType(IndividualEventType.DEATH)) {
            sb.append(", d.");
            sb.append(d.date);
            break;
        }
        return sb.toString().replace("/", "").replace("\\", "").replace("?", "");
    }

    public String createSurnameFolder()
    {
        StringBuilder sb = new StringBuilder();

        PersonalName pn = individual.names.get(0);
        sb.append( pn.surname.value);
        return sb.toString().replace("/", "").replace("\\", "").replace("?", "");
    }

    public void createBiography()throws IOException
    {
        //Folder
        String folder = createSurnameFolder();
        Path fp;
        if (WikiTreeBio.outputFolder != null)
        {
            Path folderPath = Paths.get(WikiTreeBio.outputFolder.toString(), folder);
            if(!Files.exists(folderPath))
            {
                fp = Files.createDirectory(folderPath);
            }
            else {
                fp = folderPath;
            }
        }
        else
        {
            //Use Cwd
            Path folderPath = Paths.get(folder);
            if(!Files.exists(folderPath))
            {
                fp = Files.createDirectory(folderPath);
            }
            else {
                fp = folderPath;
            }
        }

        //Create Filename
        String filename = createFilename();
        File file = new File(fp.toString(), filename + ".txt" );

        System.err.println("Creating Biography for: " + filename + " to " + file.getAbsolutePath());
        FileWriter fw = new FileWriter(file);

        StringBuilder biography = new StringBuilder();
        biography.append("\n===Biography===");

        PersonalName pn = individual.names.get(0);

        biography.append(t("Name"));
        biography.append(i(pn.givenName.value + " " + pn.surname.value));


        //Individual Birth Events at start
        addEventsOfType(biography, individual, IndividualEventType.BIRTH);
        addEventsOfType(biography, individual, IndividualEventType.BAPTISM);

        //FmailEvents - Marriage/Divorce ecetera
        List<FamilySpouse> familySpouses = individual.familiesWhereSpouse;
        for(FamilySpouse fs:familySpouses)
        {
            Family family = fs.family;
            List<FamilyEvent> fet = family.events;
            for(FamilyEvent fe:fet)
            {
                addFamilyEvent(biography, family, fe, individual);
            }
        }


        //Attributes
        addAttributeOfType(biography,individual, IndividualAttributeType.RESIDENCE);
        addAttributeOfType(biography,individual, IndividualAttributeType.OCCUPATION);
        addAttributeOfType(biography,individual, IndividualAttributeType.FACT);

        //Other events
        for (Event event : individual.events)
        {
            if (event instanceof IndividualEvent)
            {
                IndividualEvent ie = (IndividualEvent) event;
                if(!isSpeciallyProcessedEvent(ie.type))
                {
                    addEvent(biography, ie);
                }
            }
        }

        //Family Events

        //Individual Birth Events at end
        addEventsOfType(biography, individual, IndividualEventType.DEATH);
        addEventsOfType(biography, individual, IndividualEventType.BURIAL);

        biography.append("\n==Sources==");
        biography.append("\n<references/>");
        fw.write(biography.toString());
        fw.close();
    }

    private boolean isSpeciallyProcessedEvent(IndividualEventType iet)
    {
        if(iet==IndividualEventType.BIRTH ||
           iet==IndividualEventType.BAPTISM||
           iet==IndividualEventType.DEATH||
           iet==IndividualEventType.BURIAL)
        {
            return true;
        }
        return false;
    }

    private void addEventsOfType(StringBuilder biography, Individual individual, IndividualEventType eventType)
    {
        List<IndividualEvent> events = individual.getEventsOfType(eventType);
        for (IndividualEvent ie : events)
        {
            addEvent(biography, ie);
        }
    }

    private void addAttributeOfType(StringBuilder biography, Individual individual, IndividualAttributeType attributeType)
    {
        List<IndividualAttribute> attributes = individual.getAttributesOfType(attributeType);
        if(attributes.size()>0)
        {
            TreeSet<String> orderedSet = new TreeSet<String>();
            for (IndividualAttribute ia : attributes)
            {
                StringBuilder attr = new StringBuilder();
                addAttributeEvent(attr, ia);
                orderedSet.add(attr.toString());
            }

            //Now add to biography in date order
            biography.append(t(attributeType.display));
            for (String next : orderedSet)
            {
                biography.append(next);
                biography.append("\n");
            }
        }
    }

    private void addAttributeEvent(StringBuilder biography, IndividualAttribute attribute)
    {
        if (attribute.date != null)
        {
            biography.append(sti(attribute.date.toString()));
        }
        if (attribute.place != null)
        {
            biography.append(i(attribute.place.placeName));
        }
        if (attribute.address != null)
        {
            biography.append(i(attribute.address.toString()));
        }
        if (attribute.description != null && !Strings.isNullOrEmpty(attribute.description.value))
        {
            biography.append(i(attribute.description.value));
        }

        if (attribute.age != null)
        {
            biography.append(i(attribute.age.value));
        }
        if (attribute.emails != null && attribute.emails.size()>0)
        {
            biography.append(i(attribute.emails.toString()));
        }
        if (attribute.religiousAffiliation != null)
        {
            biography.append(i(attribute.religiousAffiliation.toString()));
        }

        for (AbstractCitation citation : attribute.citations)
        {
            //Add Citation
            AdvancedCitation cit = createCombinedCitationSource(citation);
            if (citations.add(cit))
            {
                biography.append(createNewReference(cit));
            }
            else
            {
                biography.append(createExistingReference(cit.getTitle()));
            }
        }
    }

    private void addEvent(StringBuilder biography, IndividualEvent ie)
    {
        biography.append(t(ie.type.display));

        if (ie.date != null)
        {
            biography.append(i(ie.date.toString()));
        }
        if(ie.place!=null)
        {
            biography.append(i(ie.place.placeName));
        }
        if(ie.description!=null)
        {
            biography.append(i(ie.description.toString()));
        }

        for (AbstractCitation citation : ie.citations)
        {
            //Add Citation
            AdvancedCitation cit = createCombinedCitationSource(citation);
            if(citations.add(cit))
            {
                biography.append(createNewReference(cit));
            }
            else
            {
                biography.append(createExistingReference(cit.getTitle()));
            }
        }
    }

    private String createNewReference(AdvancedCitation cit)
    {
        if(!Strings.isNullOrEmpty(cit.getUrl()))
        {
            return (" <ref name=\"" + cit.getTitle() + "\">" + cit.getDetails() + " [" + cit.getUrl() + " Available at ancestry]</ref>");
        }
        else
        {
            return (" <ref name=\"" + cit.getTitle() + "\">" + cit.getDetails() + "</ref>");
        }

    }

    private String createNewReference(String id, String details)
    {
        return(" <ref name=\"" + id + "\">" + details + "</ref>");
    }

    private String createExistingReference(String id)
    {
        return(" <ref name=\"" + id + "\"/>");
    }

    /**
     * Create citation, moving source infromation into citation and add to a map indexed by xref
     * @param citation
     *
     * @return null if this is a new citation
     */
    private AdvancedCitation createCombinedCitationSource(AbstractCitation citation)
    {
        if (citation instanceof CitationWithSource)
        {
            CitationWithSource cws = (CitationWithSource) citation;
            StringWithCustomTags swct = cws.whereInSource;
            if (swct != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.append(cws.source.title.get(0)+";");
                sb.append(swct.value);
                List<StringTree> sts = swct.customTags;
                for (StringTree st : sts)
                {
                    sb.append(st.value + " ");
                }
                return new AdvancedCitation(cws.source.xref, cws.source.title.get(0), sb.toString(),cws.notes);
            }
            else
            {
                return new AdvancedCitation(cws.source.xref, cws.source.title.get(0), "", cws.notes);
            }
        }
        else
        {
            CitationWithoutSource cws = (CitationWithoutSource) citation;
            return new AdvancedCitation(cws.description.toString(), cws.description.get(0), "", cws.notes);
        }
    }

    private void addFamilyEvent(StringBuilder biography, Family family, FamilyEvent ie, Individual individual)
    {


        biography.append(t(ie.type.display));

        if(family.wife!=null && family.wife!=individual)
        {
            PersonalName pn = family.wife.names.get(0);
            biography.append(i(pn.givenName + " " + pn.surname));
        }

        if(family.husband!=null && family.husband!=individual)
        {
            PersonalName pn = family.husband.names.get(0);
            biography.append(i(pn.givenName + " " + pn.surname));
        }

        if (ie.date != null)
        {
            biography.append(i(ie.date.toString()));
        }
        if(ie.place!=null)
        {
            biography.append(i(ie.place.placeName));
        }
        if(ie.description!=null)
        {
            biography.append(i(ie.description.toString()));
        }


        for (AbstractCitation citation : ie.citations)
        {
            //Add Citation
            AdvancedCitation cit = createCombinedCitationSource(citation);
            if(citations.add(cit))
            {
                biography.append(createNewReference(cit));
            }
            else
            {
                biography.append(createExistingReference(cit.getTitle()));
            }
        }
    }
}
