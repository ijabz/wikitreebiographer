# WikiTree Biographer

##Background
wikitree.org is a free site that aims to create a single genealogical tree of all of us. Data can be added either by importing a gedcom file or by manually entering the data.
When data is added via a gedcom tree it creates relationships between the people plus it then creates a wikistyle biography for each person. But the biography currently created
does not meet the current style guidelines for wikitree, for instance it does not use advanced citations.

##Objectives
This project attempts to create better biographies so that they can be copied and pasted into the people created from a gedcom import to allow better biographies to be more
easily added. If successful the ultimate aim would be for this code to be implemented into wikitrees own import facility so no then individual copy and paste would be required.

*wikitreebio* is a Java Application that takes a gedcom file and spits out a text file for each person with a biography that can be imported for that person

## Warning
This is currently very much a prototype, for instance it only adds individual attributes and events not family events such as marriage. The following wikitree profile
will be kept updated with the latest version of biographies created by wikitreebio http://www.wikitree.com/index.php?title=Hurved-108

## Requirements

*wikitreebio* requires Git and Java 1.8 for a full build and install

## Contributing

*wikitreebio* welcomes contributors, if you make an improvement or bug fix we are
very likely to merge it back into the master branch with a minimum of fuss.

## Build

Build is with [Maven](http://maven.apache.org).

- `pom.xml` : Maven build file

Directory structure as follows:

- `src`                  : source code directory
- `srctest`              : source test code directory
- `target`               : contains the `wikitreebio***dependencies.jar` built from maven


### Build details

Run

    mvn install

### Run as follows

    cd target
    java -jar wikitreebio-1.0-SNAPSHOT-jar-with-dependencies.jar gedcomtree.ged [outputfolder]
